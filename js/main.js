const startButton = document.getElementById('startButton');
const stopButton = document.getElementById('stopButton');
const debugTextArea = document.getElementById('debug-text-area');
const wall = document.getElementById('wall');
const ai = document.getElementById('ai');
const sensor_2 = document.getElementById('sensor_2');
var stepsDoneText = document.getElementById('stepsDone');
const topDebug = document.getElementById('top-debug');
const tableWallZoneCell = document.getElementById('t_wall_zone');
const tableAiZoneCell = document.getElementById('t_ai_zone');
const tableTryingZoneCell = document.getElementById('t_trying');
var c = 0;
var aiPos = 0;
var walls = 0;
var avoided = 0;
var crash = 0;
var lastWallCount = 0;
var lastAvoided = 0;
var lastCrash = 0;
var tryingZone = 0;
var tickInterval;

function tick() {
  c++;
  stepsDoneText.value = c;
  moveWall();
  checkCollision();
  experience();
}

function startSimulation() {
  if (c > 100) {
    stopSimulation();
    return;
  }

  tickInterval = setInterval(tick, 1);
}

function stopSimulation() {
  clearInterval(tickInterval);
  c = 0;
  stepsDoneText.value = c;
  wall.style.left = null;
  wall.style.right = '0';
  
  ai.style.marginTop = '50px';
  aiPos = 0;
  walls = 0;
  avoided = 0;
  crash = 0;
}

startButton.addEventListener('click', startSimulation);
stopButton.addEventListener('click', stopSimulation);

function checkWallXCoordinate(wall, wallXCoordinate) {
  if (wallXCoordinate <= 0) {
    const randomWallYPosition = Math.floor(Math.random() * (200 + 1) + 0);
    wall.style.marginTop = `${randomWallYPosition}px`;
    walls++;

    return {
      left: null,
      right: '0'
    };
  }

  return {
    left: (wallXCoordinate - 20)
  };
}

function checkWallCenter(wallCenter) {
  if (wallCenter <= 150) {
    tableWallZoneCell.innerHTML = '0';
    return 0;
  }
  
  tableWallZoneCell.innerHTML = '1';
  return 1;
}

function checkAiCenter(aiCenter) {
  if (aiCenter <= 150) {
    tableAiZoneCell.innerHTML = '0';
    return 0;
  }
  
  tableAiZoneCell.innerHTML = '1';
  return 1;
}

function moveWall() {
  const wallXCoordinate = wall.offsetLeft;
  const wallYCoordinate = wall.offsetTop;
  const aiXCoordinate = sensor_2.offsetLeft + 500;
  const aiYCoordinate = ai.offsetTop;
  const successRate = Math.floor((avoided / (avoided + crash) * 100));

  topDebug.innerHTML = `
    [${c}] <br>
    Wall (${wallXCoordinate}, ${wallYCoordinate}) <br>
    AI (${aiXCoordinate}, ${aiYCoordinate}) <br>
    Walls: ${walls} Avoided: ${avoided} Crashed: ${crash} Succes Rate: ${successRate}%
  `;


  debugTextArea.innerHTML += `
    [${c}] Wall PosX: ${wallXCoordinate} | Wall PosY: ${wallYCoordinate}
  `;
  debugTextArea.scrollTop = debugTextArea.scrollHeight;

  const {left: newLeft, right: newRight} = checkWallXCoordinate(wall, wallXCoordinate);
  wall.style.left = newLeft ? `${newLeft}px` : null;

  if (newRight) {
    wall.style.right = `${newRight}px`;
  }
}

function checkDirectionCar(direction, aiPosition) {
  if (direction === 'down') {
    return aiPosition + 10;
  }

  return aiPosition - 10
}

function moveCar(direction) {
  if (aiPos < 50) {
    aiPos = 50;
  }
  if (aiPos > 200) {
    aiPos = 200;
  }

  aiPos = checkDirectionCar(direction, aiPos);

  ai.style.marginTop = `${aiPos}px`; 
}


function checkCollision() {
  const wallXCoordinate = wall.offsetLeft;
  const wallYCoordinate = wall.offsetTop + 100;
  const aiXCoordinate = sensor_2.offsetLeft + 500;
  const aiYCoordinate = ai.offsetTop;

  debugTextArea.innerHTML += `
    [${c}] Sensor2 PosX: ${aiXCoordinate} | PosY: ${aiYCoordinate}
  `;

  if (
    wallXCoordinate < aiXCoordinate &&
    aiYCoordinate >= (wallYCoordinate - 100) &&
    aiYCoordinate < wallYCoordinate ||
    wallXCoordinate < aiXCoordinate &&
    (wallYCoordinate - 100) > aiYCoordinate &&
    (wallYCoordinate - 100) < (aiYCoordinate + 50)
    ) {
    moveCar('down');
    sensor_2.style.backgroundColor = 'red';

    if (wallXCoordinate < 100) {
      crash++;
    }

    return;
  }

  sensor_2.style.backgroundColor = 'white';

  if (wallXCoordinate < 100) {
    avoided++;
  }

  return;
}

function checkTryingZone(tryingZone) {
  if (tryingZone === 0) {
    moveCar('up');
    return;
  }

  moveCar('down');
  return;
}

function experience() {
  const wallYCoordinate = wall.offsetTop + 100;
  const aiYCoordinate = ai.offsetTop;
  const wallCenter = wallYCoordinate + 50;
  const aiCenter = aiYCoordinate - 75;
  const wallZone = checkWallCenter(wallCenter);
  const aiZone = checkAiCenter(aiCenter);

  tableTryingZoneCell.innerHTML = tryingZone;

  let buildVar = `${aiZone}${wallZone}${tryingZone}`;
  let buildVarElement = document.getElementById(`succ_${buildVar}`);
  let experienceDB = buildVarElement.innerHTML;

  if (tryingZone === 0) {
    const buildVarOther = `${aiZone}${wallZone}1`;
    const buildVarElementOther = document.getElementById(`succ_${buildVarOther}`);
    const experienceDBOther = buildVarElementOther.innerHTML;

    if (parseInt(experienceDBOther) > parseInt(experienceDB) + parseInt(10)) {
      buildVar = buildVarOther;
      buildVarElement = document.getElementById(`succ_${buildVar}`);
      experienceDB = buildVarElement.innerHTML;
      tryingZone = 1;
    }
  }

  if (tryingZone === 1) {
    const buildVarOther = `${aiZone}${wallZone}0`;
    const buildVarElementOther = document.getElementById(`succ_${buildVarOther}`);
    const experienceDBOther = buildVarElementOther.innerHTML;

    if (parseInt(experienceDBOther) > parseInt(experienceDB) + parseInt(10)) {
      buildVar = buildVarOther;
      buildVarElement = document.getElementById(`succ_${buildVar}`);
      experienceDB = buildVarElement.innerHTML;
      tryingZone = 0;
    }
  }

  checkTryingZone(tryingZone);

  if (lastWallCount !== walls) {
    if (lastAvoided !== avoided) {
      experienceDB = parseInt(experienceDB) + parseInt(1);
      buildVarElement.innerHTML = experienceDB;
      lastAvoided = avoided;
    }
  
    if (lastCrash !== crash) {
      experienceDB = parseInt(experienceDB) - parseInt(1);
      buildVarElement.innerHTML = experienceDB;
      lastCrash = crash;
    }

    lastWallCount = walls;
    tryingZone = Math.floor(Math.random() * 2);

    moveWall();
  }

}